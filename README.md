# b3_c1_jenkins

Ce repo montre la config d'un Jenkins qui sert à build le repo trouvable à l'adresse suivante : [Voxelsniper](https://github.com/Aime23/VoxelSniper)

## Pipeline Jenkins : 
```
pipeline {
    agent any

    tools {
        // Install the Maven version configured as "M3" and add it to the path.
        maven "M3"
    }

    stages {
        stage("Clone") {
            steps {
                git "https://github.com/Aime23/VoxelSniper.git"
            }
        }
        
        stage('Build') {
            steps {
                sh "mvn -Dmaven.test.failure.ignore=true clean package"

            }

            post {
                // If Maven was able to run the tests, even if some of the test
                // failed, record the test results and archive the jar file.
                success {
                    junit '**/target/surefire-reports/TEST-*.xml'
                    archiveArtifacts 'target/*.jar'
                }
                 always {
                    discordSend description: "Build : " + currentBuild.currentResult, link: env.BUILD_URL, result: currentBuild.currentResult, title: JOB_NAME, webhookURL: "https://discord.com/api/webhooks/1095334469189238855/zRDbfmOF44EB1D8daVU4qaQmcCeVnkDpNhxNL5UxnIXZGFVuYgZt3-WNQQ2os5Zycn9S"
                }

            }
        }
    }
}
```
Le pipeline comporte 2 stages très simples, le premier qui a pour but de cloner le répository et le second stage qui lance les tests et le build du projet, ce stage retourne les artefacts de build ainsi que les résultats des tests.

Le pipeline est exécuté à chaque push de code sur le repo, il envoi aussi un message sur discord avec le résultat du build  
![Notification discord](/pictures/discord.png "Discord") 

![Overview du résultat du panneau jenkins](/pictures/overview.png "Overview")

![Liste des settings](/pictures/settings.png "Setting")

### Liste des plugins
* Discord Notifier
* Github Plugin

### Configuration des tools

![Config de maven](/pictures/maven.png "Maven")